const express = require("express");
const UserController = require("../controllers/userController.js");
const userRouter = express.Router();

userRouter.get("/", UserController.getAllUsers);
userRouter.get("/:username", UserController.getIndividualUserDetails);
userRouter.delete("/:username", UserController.deleteUser);
userRouter.put("/:username", UserController.updateUser);

module.exports = userRouter;
