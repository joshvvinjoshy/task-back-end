const express = require("express");
const SignupController = require("../controllers/signupController.js");
const signupRouter = express.Router();

signupRouter.post("/", SignupController.addUser);
module.exports = signupRouter;
