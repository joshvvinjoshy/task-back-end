const express = require("express");
const LoginController = require("../controllers/loginController.js");
const loginRouter = express.Router();

loginRouter.post("/", LoginController.login);
module.exports = loginRouter;
