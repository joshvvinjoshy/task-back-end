const express = require("express");
const UploadController = require("../controllers/uploadController.js");
const uploadRouter = express.Router();

uploadRouter.post("/", UploadController.uploadFile);

module.exports = uploadRouter;
