require("dotenv").config();
const db = {
  host: process.env.VITE_LOCALHOST,
  user: process.env.VITE_USERNAME,
  password: process.env.VITE_PASSWORD,
  database: process.env.VITE_DATABASE,
};
module.exports = db;
