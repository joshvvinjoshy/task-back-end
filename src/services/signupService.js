const { query } = require("./db.service");
const bcrypt = require("bcrypt");

async function addUser(reqbody) {
  const saltRounds = 10;
  const hashedPassword = await bcrypt.hash(reqbody.password, saltRounds);
  const rows = await query(
    `insert into logindetails values('${reqbody.username}', '${reqbody.emailaddress}', '${hashedPassword}', '${reqbody.gender}', '${reqbody.dateofbirth}')`
  );
  let message = `User with username ${reqbody.username} already exists`;
  if (rows) {
    message = "New User added successfully";
  }
  return message;
}

module.exports = { addUser };
