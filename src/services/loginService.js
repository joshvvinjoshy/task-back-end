const { query } = require("./db.service");
const bcrypt = require("bcrypt");

async function login(reqbody) {
  const rows = await query(
    `select * from logindetails where username ='${reqbody.username}'`
  );
  let message = `Failed to fetch details of user with username ${reqbody.username}`;
  if (rows) {
    const match = await bcrypt.compare(reqbody.password, rows[0].password);
    if (match) {
      message = "Authentication successfull";
    } else {
      message = "Authentication failed";
    }
  }
  return message;
}
module.exports = { login };
