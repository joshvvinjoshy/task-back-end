const mysql = require("mysql");
const util = require("util");
const dbConfig = require("../config/db.config");
const connection = mysql.createConnection(dbConfig);
connection.connect((error) => {
  if (error) {
    console.error(error);
  } else {
    console.log("Connected to the database");
  }
});
const query = util.promisify(connection.query).bind(connection);
module.exports = { query };
