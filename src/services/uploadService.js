const { query } = require("./db.service");
const fs = require("fs");
const bcrypt = require("bcrypt");
const moment = require("moment");
async function uploadFile(reqbody) {
  async function encryptPassword(username) {
    const saltRounds = 10;
    const hashedPassword = await bcrypt.hash(username, saltRounds);
    return hashedPassword;
  }
  const filename = reqbody.filename.trim();
  const file = `/home/joshvvinjoshy/Downloads/${filename}`;
  fs.readFile(file, "utf-8", async (err, data) => {
    if (err) {
      console.log(err);
    } else {
      const csv_lines = data.split("\n");
      const csv_headers = csv_lines[0].split("/n")[0].split(",");
      const csv_data = csv_lines.slice(1).map((line) => {
        return line.split(",").reduce((acc, cur, i) => {
          const toAdd = {};
          toAdd[csv_headers[i]] = cur;
          return { ...acc, ...toAdd };
        }, {});
      });
      try {
        const rows = await query(`select emailaddress from logindetails`);
        if (rows) {
          const totalquery = await csv_data
            .filter((csv_user) => {
              return csv_user.Username != "";
            })
            .reduce(async (accquery, csv_user) => {
              const found = rows.some((userEmail) => {
                return userEmail.emailaddress == csv_user.Email;
              });
              if (!found) {
                const hashedPassword = await encryptPassword(csv_user.Username);
                const momentObject = moment(
                  csv_user["Date of Birth"],
                  "DD/MM/YYYY"
                );
                const momentDate = momentObject.toDate();
                if ((await accquery) == "") {
                  const tempquery = `insert into logindetails values('${
                    csv_user.Username
                  }', '${csv_user.Email}', '${hashedPassword}', '${
                    csv_user.Gender
                  }', '${momentDate.toString()}')`;
                  return (await accquery) + tempquery;
                } else {
                  const tempquery = `, ('${csv_user.Username}', '${
                    csv_user.Email
                  }', '${hashedPassword}', '${
                    csv_user.Gender
                  }', '${momentDate.toString()}')`;

                  return (await accquery) + tempquery;
                }
              } else {
                return await accquery;
              }
            }, "");
          if (totalquery) {
            const result = await query(totalquery);
            if (result) {
              return result;
            }
          }
        }
      } catch (err) {
        console.error(err);
      }
    }
  });
}

module.exports = { uploadFile };
