const { query } = require("./db.service");

async function getAll() {
  const rows = await query("select * from logindetails");
  if (rows) {
    return rows;
  }
}

async function getIndividualUserDetails(username) {
  const rows = await query(
    `select * from logindetails where username='${username}'`
  );
  if (rows) {
    return rows;
  }
}
async function deleteUser(username) {
  const rows = await query(
    `delete from logindetails where username='${username}'`
  );
  let message = "";
  if (rows) {
    message = `User with username ${username} has been deleted successfully `;
  }
  return message;
}

async function updateUser(reqbody, username) {
  const rows = await query(
    `update logindetails set gender= '${reqbody.gender}', dateofbirth= '${reqbody.dateofbirth}' where username= '${username}'`
  );
  let message = `Failed to update user with username ${username}`;
  if (rows) {
    message = `User with username ${username} has been updated successfully `;
  }
  return message;
}
module.exports = {
  getAll,
  getIndividualUserDetails,
  deleteUser,
  updateUser,
};
