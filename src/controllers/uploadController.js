const UploadService = require("../services/uploadService.js");

async function uploadFile(req, res) {
  try {
    const data = await UploadService.uploadFile(req.body);
    res.status(201).send(data);
  } catch (err) {
    res.status(500).send("Failed to import users");
  }
}

module.exports = { uploadFile };
