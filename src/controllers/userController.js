const UserService = require("../services/userService.js");

async function getAllUsers(req, res) {
  try {
    const data = await UserService.getAll();
    res.status(200).send(data);
  } catch (err) {
    res.send(500).send("Failed to fetch data");
  }
}

async function getIndividualUserDetails(req, res) {
  try {
    const data = await UserService.getIndividualUserDetails(
      req.params.username
    );
    res.status(200).send(data);
  } catch (err) {
    res
      .send(500)
      .send(
        `Failed to fetch details of the user with username ${reqbody.username}`
      );
  }
}

async function deleteUser(req, res) {
  try {
    const data = await UserService.deleteUser(req.params.username);
    res.status(200).send(data);
  } catch (err) {
    res
      .send(500)
      .send(`Failed to delete user with username ${req.params.username}`);
  }
}

async function updateUser(req, res) {
  try {
    const data = await UserService.updateUser(req.body, req.params.username);
    res.status(200).send(data);
  } catch (err) {
    res
      .status(500)
      .send(`Failed to update user with username ${req.params.username}`);
  }
}

module.exports = {
  getAllUsers,
  getIndividualUserDetails,
  deleteUser,
  updateUser,
};
