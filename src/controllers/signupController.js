const SignupService = require("../services/signupService.js");

async function addUser(req, res) {
  try {
    const data = await SignupService.addUser(req.body);
    console.log(data);
    res.status(201).send(data);
  } catch (err) {
    if (err.message.includes("logindetails.emailaddress")) {
      res.status(409).send(`User with emailaddress already exists`);
    } else {
      res.status(409).send("User with username already exists");
    }
  }
}
module.exports = { addUser };
