const LoginService = require("../services/loginService.js");

async function login(req, res) {
  try {
    const data = await LoginService.login(req.body);
    if (data == "Authentication failed") {
      throw new Error(data);
    }
    res.status(200).send(data);
  } catch (err) {
    res.status(401).send(err.message);
  }
}

module.exports = { login };
