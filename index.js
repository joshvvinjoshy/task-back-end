const express = require("express");
const cors = require("cors");
const userRouter = require("./src/routes/users.js");
const loginRouter = require("./src/routes/login.js");
const signupRouter = require("./src/routes/signup.js");
const uploadRouter = require("./src/routes/upload.js");
const app = express();
const multer = require("multer");
const upload = multer();
const port = process.env.PORT || 8000;
const fs = require("fs");
const bcrypt = require("bcrypt");
const { query } = require("./src/services/db.service.js");

// const { connection } = require("./databaseDetails.js");

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors({ origin: "*" }));
app.use("/users", userRouter);
app.use("/login", loginRouter);
app.use("/signup", signupRouter);
app.use("/uploadfile", upload.single("file"), uploadRouter);

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
